# Introduction to AI - Practical Component 2 - Chess AI #

This repository contains my work for on implementing AI in a chess game.
The original codebase was provided by a lecturer. My work has been focused on improving the provided code and implementing the AI portion.