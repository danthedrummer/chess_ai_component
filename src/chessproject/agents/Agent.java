package chessproject.agents;

import chessproject.Move;

import java.util.Stack;

public interface Agent {
    double PAWN_VALUE = 1;
    double KNIGHT_VALUE = 3;
    double BISHOP_VALUE = 3.5;
    double ROOK_VALUE = 5;
    double QUEEN_VALUE = 9;
    double KING_VALUE = Double.MAX_VALUE;
    Move makeMove(Stack<Move> possibilities);
}
