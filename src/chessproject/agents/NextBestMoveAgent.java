package chessproject.agents;

import chessproject.Move;
import chessproject.Turn;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

class NextBestMoveAgent implements Agent {

    private Agent defaultAgent;

    NextBestMoveAgent() {
        defaultAgent = new RandomAgent();
    }

    /**
     * Determines the best move that the AI can make this turn
     * without regard for retaliation from opponent
     *
     * @param possibilities A stack of possible moves that can be made
     * @return The best possible move to be made this turn
     */
    @Override
    public Move makeMove(Stack<Move> possibilities) {
        Stack<Move> copy = (Stack)possibilities.clone();
        List<Move> bestMove = determineNumberOfTopMoves(possibilities, 1, Turn.WHITE);
        if (bestMove.get(0).getValue() == -1) {
            System.out.println("No pieces to take - Defaulting to random move");
            return defaultAgent.makeMove(copy);
        }
        return bestMove.get(0);
    }

    /**
     * Determines a number of best moves that can be made based on the
     * scoring mechanism implemented
     *
     * The reason for having this method is for the next agent attempting to
     * examine multiple options instead of the best move for this current action
     *
     * @param possibilities All the possible moves that can be made
     * @param numberOfMoves The number of top moves to be considered
     * @param player The player making the move
     * @return A stack of best moves limited by the amount specified
     */
    List<Move> determineNumberOfTopMoves(Stack<Move> possibilities, final int numberOfMoves, Turn player) {
        List<Move> bestMoves = new ArrayList<Move>(){{
            for (int i = 0; i < numberOfMoves; i++) {
                add(null);
            }
        }};

        while (!possibilities.empty()) {
            Move move = possibilities.pop();
            move.setValue(evaluateMove(move, player));
            for (int i = 0; i < bestMoves.size(); i++) {
                if (bestMoves.get(i) == null) {
                    bestMoves.set(i, move);
                    break;
                }
                if (move.getValue() >= bestMoves.get(i).getValue()) {
                    bestMoves.add(i, move);
                    break;
                }
            }
            //Trim the end of the list until it's the right size
            while (bestMoves.size() > numberOfMoves) {
                bestMoves.remove(bestMoves.size()-1);
            }
        }

        return bestMoves;
    }

    /**
     * Determines the value of a move based on the scoring system implemented
     * Returns -1 if the move has no measurable advantage gain
     *
     * @param move The move to be evaluated
     * @param player The player making the move
     * @return the value of the move
     */
    private double evaluateMove(Move move, Turn player) {
        String pieceToTake = move.getLanding().getName();

        if (!pieceToTake.contains(player.getPlayerName())) {
            if (pieceToTake.contains("Pawn")) {
                return PAWN_VALUE;
            } else if (pieceToTake.contains("Rook")) {
                return ROOK_VALUE;
            } else if (pieceToTake.contains("Bishop")) {
                return BISHOP_VALUE;
            } else if (pieceToTake.contains("Knight")) {
                return KNIGHT_VALUE;
            } else if (pieceToTake.contains("Queen")) {
                return QUEEN_VALUE;
            } else if (pieceToTake.contains("King")) {
                return KING_VALUE;
            }
        }

        return -1;
    }
}
