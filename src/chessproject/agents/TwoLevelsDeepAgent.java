package chessproject.agents;

import chessproject.Move;

import java.util.Stack;

class TwoLevelsDeepAgent implements Agent {

    private Agent defaultAgent = new RandomAgent();

    TwoLevelsDeepAgent() { }

    /**
     * Determines the best move that the AI can make this turn
     * taking into consideration retaliation from opponent and potential
     * moves that can be made the following turn
     *
     * @param possibilities A stack of possible moves that can be made
     * @return The best possible move to be made with all considerations
     */
    @Override
    public Move makeMove(Stack<Move> possibilities) {
        //TODO: implement
        System.out.println(getClass().toString() + " is yet to be implemented\nDefaulting to random moves");
        return defaultAgent.makeMove(possibilities);
    }
}
