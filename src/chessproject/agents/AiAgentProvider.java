package chessproject.agents;

/**
 * This class provides the implemented AI {@link Agent}s
 */
public enum AiAgentProvider {
    RANDOM,
    NEXT_BEST_MOVE,
    TWO_LEVELS_DEEP;

    /**
     * Returns the appropriate AI {@link Agent} based on the enum value accessed
     * @return An AI {@link Agent}
     */
    public Agent getAiAgent() {
        switch (this.toString()) {
            case "RANDOM":
                return new RandomAgent();
            case "NEXT_BEST_MOVE":
                return new NextBestMoveAgent();
            case "TWO_LEVELS_DEEP":
                return new TwoLevelsDeepAgent();
            default:
                return new RandomAgent();
        }
    }

    /**
     * Returns a list of the enum values in regular case
     * @return a string array of the enum values
     */
    public static String[] valuesForAiSelection() {
        AiAgentProvider[] agents = AiAgentProvider.values();
        String[] agentNames = new String[agents.length];

        for (int i = 0; i < agents.length; i++) {
            char[] charArray = agents[i].toString().toCharArray();
            StringBuilder playerName = new StringBuilder(Character.toString(charArray[0]));
            for (int j = 1; j < charArray.length; j++) {
                if (charArray[j] == '_') {
                    charArray[j] = ' ';
                    playerName.append(Character.toString(charArray[j]));
                    j++;
                } else {
                    charArray[j] = Character.toLowerCase(charArray[j]);
                }
                playerName.append(Character.toString(charArray[j]));

            }
            agentNames[i] = playerName.toString();
        }

        return agentNames;
    }
}
