package chessproject.agents;

import chessproject.Move;

import java.util.Random;
import java.util.Stack;

class RandomAgent implements Agent {

    private Random rand;

    RandomAgent() {
        this.rand = new Random();
    }

    /**
     * The method randomMove takes as input a stack of potential moves that the AI agent
     * can make. The agent uses a random number generator to randomly select a move from
     * the inputted Stack and returns this to the calling agent.
     *
     * @param possibilities A stack of possible moves that can be made
     * @return A random move from the stack of possible moves
     */
    @Override
    public Move makeMove(Stack<Move> possibilities) {
        int moveID = rand.nextInt(possibilities.size());
        for (int i = 1; i < (possibilities.size() - (moveID)); i++) {
            possibilities.pop();
        }
        return possibilities.pop();
    }
}
