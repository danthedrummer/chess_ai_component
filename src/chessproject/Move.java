package chessproject;

/**
 * Model describing the movement of a piece from one position to another
 */
public class Move {

    private Square start;
    private Square landing;
    private double value;

    public Move() { }

    Move(Square start, Square landing) {
        this.start = start;
        this.landing = landing;
        this.value = 0;
    }

    /**
     * Get the starting square of this move
     * @return The starting square
     */
    public Square getStart() {
        return start;
    }

    /**
     * Get the ending square of this move
     * @return The ending square
     */
    public Square getLanding() {
        return landing;
    }

    public void setValue(double score) {
        this.value = score;
    }

    public double getValue() {
        return value;
    }
}
