package chessproject;

/**
 * Model describing a square on the board
 */
public class Square {

    private int xIndex;
    private int yIndex;
    private String pieceName;

    Square(int xIndex, int yIndex, String name) {
        this.xIndex = xIndex;
        this.yIndex = yIndex;
        pieceName = name;
    }

    Square(int xIndex, int yIndex) {
        this.xIndex = xIndex;
        this.yIndex = yIndex;
        pieceName = "";
    }

    /**
     * Get the x index of this square
     * @return The x index
     */
    int getXIndex() {
        return xIndex;
    }

    /**
     * Get the y index of this square
     * @return The y index
     */
    int getYIndex() {
        return yIndex;
    }

    /**
     * Get the name of the piece occupying this square
     * @return The piece name, or empty string if there is no piece
     */
    public String getName() {
        return pieceName;
    }
}
