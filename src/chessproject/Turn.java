package chessproject;

/**
 * Enum describing whose turn it currently is
 */
public enum Turn {
    WHITE,
    BLACK;

    /**
     * Gets the name of the player in regular case
     * @return the players name in regular case
     */
    public String getPlayerName() {
        char[] charArray = this.toString().toCharArray();
        StringBuilder playerName = new StringBuilder(Character.toString(charArray[0]));
        for (int i = 1; i < charArray.length; i++) {
            charArray[i] = Character.toLowerCase(charArray[i]);
            playerName.append(Character.toString(charArray[i]));
        }
        return playerName.toString();
    }
}
